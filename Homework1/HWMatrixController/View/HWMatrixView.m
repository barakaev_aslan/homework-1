//
//  HWMatrixView.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWMatrixView.h"
#import "HWRoundRectButton.h"
#import "HWNumericTextField.h"

@interface HWMatrixView ()

@property (nonatomic, strong) NSArray<NSArray<UILabel *> *> *matrixLabels;

@end

@implementation HWMatrixView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];

        _contentInsets = UIEdgeInsetsMake(HWDefaultMargin,
                                          HWDefaultMargin,
                                          HWDefaultMargin,
                                          HWDefaultMargin);
        
        _rowsCountInputTextFiled = [[HWNumericTextField alloc] init];
        _rowsCountInputTextFiled.placeholder = NSLocalizedString(@"matrix.input.rows.placeholder", nil);
        _rowsCountInputTextFiled.returnKeyType = UIReturnKeyNext;
        [self addSubview:_rowsCountInputTextFiled];
        
        _columnsCountInputTextField = [[HWNumericTextField alloc] init];
        _columnsCountInputTextField.placeholder = NSLocalizedString(@"matrix.input.columns.placeholder", nil);
        [self addSubview:_columnsCountInputTextField];
        
        _createButton = [[HWRoundRectButton alloc] init];
        [_createButton setTitle:NSLocalizedString(@"matrix.input.button.title", nil) forState:UIControlStateNormal];
        [self addSubview:_createButton];
        
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.bounces = NO;
        [self addSubview:_scrollView];
        
        _emptyLabel = [[UILabel alloc] init];
        _emptyLabel.font = [UIFont systemFontOfSize:16.0f];
        _emptyLabel.text = NSLocalizedString(@"matrix.empty.text", nil);
        _emptyLabel.numberOfLines = 0;
        _emptyLabel.hidden = YES;
        [self addSubview:_emptyLabel];
    }
    return self;
}

#pragma mark - Accessors

- (void)setContentInsets:(UIEdgeInsets)contentInsets {
    _contentInsets = contentInsets;
    [self setNeedsLayout];
}

#pragma mark - Public methods

- (void)setMatrix:(NSArray<NSArray<NSNumber *> *> *)matrix {
    [self.matrixLabels enumerateObjectsUsingBlock:^(NSArray<UILabel *> * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }];
    self.matrixLabels = nil;
    
    self.emptyLabel.hidden = matrix.count > 0;
    
    NSMutableArray<NSArray<UILabel *> *> *mutableMatrixLabels = [[NSMutableArray alloc] init];
    UIFont *labelFont = [UIFont systemFontOfSize:20.f];
    
    [matrix enumerateObjectsUsingBlock:^(NSArray<NSNumber *> * _Nonnull row, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableArray<UILabel *> *rowLabels = [[NSMutableArray alloc] initWithCapacity:row.count];
        [row enumerateObjectsUsingBlock:^(NSNumber * _Nonnull element, NSUInteger idx, BOOL * _Nonnull stop) {
            UILabel *label = [[UILabel alloc] init];
            label.font = labelFont;
            label.text = element.stringValue;
            [rowLabels addObject:label];
            [self.scrollView addSubview:label];
        }];
        [mutableMatrixLabels addObject:[rowLabels copy]];
    }];
    self.matrixLabels = [mutableMatrixLabels copy];
    [self setNeedsLayout];
}

#pragma mark - Override

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect actualBounds = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
    CGFloat inputHeight = HWDefaultInputHeight;
    CGFloat offset = HWDefaultMargin;
    
    CGSize createButtonSize = [self.createButton sizeThatFits:CGSizeMake(actualBounds.size.width, inputHeight)];
    CGFloat textFieldWidht = (actualBounds.size.width - createButtonSize.width - offset * 2.0f) / 2.0f;
    
    self.rowsCountInputTextFiled.frame = CGRectMake(actualBounds.origin.x,
                                           actualBounds.origin.y,
                                           textFieldWidht,
                                           inputHeight);
    
    self.columnsCountInputTextField.frame = CGRectMake(CGRectGetMaxX(self.rowsCountInputTextFiled.frame) + offset,
                                                       actualBounds.origin.y,
                                                       textFieldWidht,
                                                       inputHeight);
    
    self.createButton.frame = CGRectMake(CGRectGetMaxX(actualBounds) - createButtonSize.width,
                                          actualBounds.origin.y,
                                          createButtonSize.width,
                                          inputHeight);
    
    
    

    
    if (self.matrixLabels.count > 0) {
        __block CGSize contentSize = CGSizeZero;
        __block CGPoint position = CGPointZero;
        
        [self.matrixLabels enumerateObjectsUsingBlock:^(NSArray<UILabel *> * _Nonnull rowLabels, NSUInteger idx, BOOL * _Nonnull stop) {
            __block CGFloat maxHeight = 0.0f;
            [rowLabels enumerateObjectsUsingBlock:^(UILabel * _Nonnull label, NSUInteger idx, BOOL * _Nonnull stop) {
                CGSize labelSize = [label sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
                label.frame = CGRectMake(position.x,
                                         position.y,
                                         labelSize.width,
                                         labelSize.height);
                position.x += labelSize.width + offset;
                maxHeight = MAX(maxHeight, labelSize.height);
            }];
            contentSize.width = MAX(contentSize.width, position.x - offset);
            position.y += maxHeight + offset;
            position.x = 0.0f;
        }];
        contentSize.height = position.y - offset;
        
        self.scrollView.frame = CGRectMake(actualBounds.origin.x,
                                           actualBounds.origin.y + HWDefaultInputHeight + HWDefaultMargin,
                                           actualBounds.size.width,
                                           MIN(actualBounds.size.width, contentSize.height));
        self.scrollView.contentSize = contentSize;
    }
    else {
        self.scrollView.frame = CGRectZero;
        self.scrollView.contentSize = CGSizeZero;
        
        CGSize emptyLabelSize = self.emptyLabel.hidden ? CGSizeZero : [self.emptyLabel sizeThatFits:CGSizeMake(actualBounds.size.width, CGFLOAT_MAX)];
        self.emptyLabel.frame = CGRectMake(actualBounds.origin.x,
                                           actualBounds.origin.y + HWDefaultInputHeight + HWDefaultMargin,
                                           actualBounds.size.width,
                                           emptyLabelSize.height);
    }

}

- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat height = self.contentInsets.top + HWDefaultInputHeight + self.contentInsets.bottom;
    
    __block CGFloat additionalHeight = 0.0f;

    if (self.matrixLabels.count > 0) {
        [self.matrixLabels enumerateObjectsUsingBlock:^(NSArray<UILabel *> * _Nonnull rowLabels, NSUInteger idx, BOOL * _Nonnull stop) {
            __block CGFloat maxHeight = 0.0f;
            [rowLabels enumerateObjectsUsingBlock:^(UILabel * _Nonnull label, NSUInteger idx, BOOL * _Nonnull stop) {
                maxHeight = MAX(maxHeight, [label sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)].height);
            }];
            additionalHeight += maxHeight > 0.0f ? maxHeight + HWDefaultMargin : 0.0f;
        }];
        additionalHeight = MIN(additionalHeight, size.width);
    }
    else {
        additionalHeight = self.emptyLabel.hidden ? 0.0f : ([self.emptyLabel sizeThatFits:CGSizeMake(size.width - self.contentInsets.left - self.contentInsets.right, CGFLOAT_MAX)].height + HWDefaultMargin);
    }

    return CGSizeMake(size.width, height + additionalHeight);
}

@end
